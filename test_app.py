import unittest
import requests

url = "http://13.209.15.210:8047"
add_url = url + '/add'
sub_url = url + '/sub'

class TestApp(unittest.TestCase):
    def test_add(self):
        params = {
            "a": "1", 
            "b": "3"
        }
        res = requests.get(add_url, params=params)
        r_res = int(res.text)
        self.assertEqual(r_res, 4)

    def test_sub(self):
        params = {
            "a": "4", 
            "b": "4"
        }
        res = requests.get(sub_url, params=params)
        r_res = int(res.text)
        self.assertEqual(r_res, 0)

if __name__ == "__main__":
    unittest.main()

