from flask import Flask
from flask import request
app = Flask(__name__)

@app.route("/")
def index():
    return "Hello BOB from Ho9"

@app.route("/add")
def add():
    add_num1 = int(request.args.get('a', 0))
    add_num2 = int(request.args.get('b', 0))
    return str(add_num1 + add_num2)

@app.route("/sub")
def sub():
    sub_num1 = int(request.args.get('a', 0))
    sub_num2 = int(request.args.get('b', 0))
    return str(sub_num1 - sub_num2)

if __name__ == "__main__":
    app.run('0.0.0.0', port=8047)
